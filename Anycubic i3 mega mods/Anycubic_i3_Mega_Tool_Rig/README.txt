                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:2793693
Anycubic i3 Mega Tool Rig by mjuddi is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

This is a tool rig for the Anycubic i3 Mega. It has slots for all tools that come with the printer as well as a slide for an extra SD card and a bowl that can be used to store small items, f.x. filament clips etc. I haven't tried printing it without supports, so I can only recommend printing it with supports. Also the settings are pretty relaxed, you can experiment with other settings as well.

UPDATE 15.01.19:
Added a new version with a single screwdriver hole for newer version of the i3 Mega.

# Print Settings

Printer: Anycubic i3 Mega
Rafts: No
Supports: Yes
Resolution: 0.2
Infill: 20%

Notes: 
Shell thickness: 1.2mm

UPDATE:
IF YOU USE CURA, GO TO LAYER VIEW BEFORE YOU PRINT THE OBJECT AND MAKE SURE ALL THE HOLES ARE PRINTED. 
MYSELF AND COUNTLESS OTHER PEOPLE HAVE SUCCESSFULLY PRINTED THIS OBJECT WITH ALL THE HOLES, INCLUDING THE ONES FOR THE SCREWDRIVERS. PLEASE CHECK YOUR SETTINGS IN CURA AND MAKE SURE THAT "REMOVE ALL HOLES" OR "UNION OVERLAPPING VOLUMES" IS NOT CHECKED UNDER "MESH FIXES". YOU COULD ALSO GOOGLE SEARCH FOR "CURA FILLS HOLES" AND TRY OTHER SETTINGS IF THIS IS NOT YOUR PROBLEM. 

BUT MOST IMPORTANTLY!!! CHECK LAYER-VIEW BEFORE PRINTING. THANKS.

# How I Designed This

Created with Blender, mixed the tool box design from Bogdanko's incredibly solid Anycubic i3 Mega Led Holder set and used derLowy's mount to screw it on to the printer. Then I arranged Bogdanko's setup in a different way that fits my needs more.