                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2869065
Anycubic i3 Mega Filament clip guide by erikburgman is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

For my Anycubic i3 Mega, I use the custom filament holder that clicks at the right site instead of the separate original holder which I hate. But even when you use the original one, the filament goes through the filament sensor which comes off if tension from the side occurs. So, I designed this filament guide that clicks on (no screw to losen!). It grips at the bottom and over the little screw on the top of the right motor chassis. Now the filament is going up straight to the sensor. This is version 2 (V2) which has a little pipe so the filament can't escape. Just print it without any supports. Enjoy!

P.S. The picture shows the clip guide bringing the filament to the sensor mounted on my custom sensor holder that you can find on Thingiverse also.

# Print Settings

Printer: Anycubic i3 Mega
Rafts: No
Supports: No
Resolution: 0.2
Infill: 25%