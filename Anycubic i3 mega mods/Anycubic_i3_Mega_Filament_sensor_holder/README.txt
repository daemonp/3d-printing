                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2869396
Anycubic i3 Mega Filament sensor holder by erikburgman is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

The Anycubic i3 Mega is a great 3D printer with a filament sensor. But that sensor is mounted with a magnet and comes of very often during printing. Then it winds up to the extruder and with each filament short release, you hear it ticking. very Annoying!

First I designed an sensor extender that gave a better aligned from the filament to the extruder still using the standard holder with the magnet. You find it elsewhere on Thingiverse. But sill the sensor came loose from time to time with this extender. Ahrggg!

So I designed a new holder. This holder replaces the standard holder with magnet and aligns the incoming filament perfectly with the extruder. However, you need 2 little screws and nuts to mount the filament sensor to this new sensor holder. The screws are about 2cm long and 3mm thick and go through the 2 holes from the new holder and through the existing 2 holes from the sensor. You also could use 2 tiny tyraps instead of screws.

This is the only way to solve this sensor problem for sure. Installation is easy; remove the holder with magnet and use the same screw to attach my holder at the same place. Store the original one with your other i3 mega stuff.

I suggest that you use my sensor holder in combination with my filament click guide which you also can find on Thingiverse.

# Print Settings

Printer: Anycubic i3 Mega
Rafts: No
Supports: No
Resolution: 0.2
Infill: 25%