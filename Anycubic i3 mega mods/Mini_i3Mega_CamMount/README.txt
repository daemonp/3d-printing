                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2824288
Mini_i3Mega_CamMount by Lizard_Wizard is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

My third cam mount design.  Made to be printed on its side, so its print friendly :)
This has nice height to the cam mount.  Will upload a video of it in action soon.

Model being printed:
https://youtu.be/2xHGNWn1tWY

Camera mount in action:
https://youtu.be/5ts2F3uz2XE






# Print Settings

Printer: Anycubic i3 MEGA
Rafts: No
Supports: No
Resolution: .2
Infill: 20

Notes: 
Print on its side.

# Post-Printing

<iframe src="//www.youtube.com/embed/2xHGNWn1tWY" frameborder="0" allowfullscreen></iframe>
This is just the part being printed.  The camera mount used was my v2 thing: 2824113