                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:2458308
Anycubic i3 Mega webcam holder by tierradenadie is licensed under the Creative Commons - Attribution - Non-Commercial - Share Alike license.
http://creativecommons.org/licenses/by-nc-sa/3.0/

# Summary

Inspired by the Raspi camera holder for the i3 Mega (https://www.thingiverse.com/thing:2275881) I designed a holder for my webcam, I use to monitor my prints with OctoPrint. This way the growing of the part in timelapse video is much smoother to view, as if the camera was at a fixed position.

The holder needs no screws and is just clamped to the sheet metal below the print plate.

A demonstration video of the 20 mm hollow calibration cube (https://www.thingiverse.com/thing:271736) can be found here: https://youtu.be/Uj4-BXuUzgA)

UPDATE 2017-08-18: After some weeks of used I encounterd a problem with this holder. The plastic deformed a bit and blocked the movement of the heatbed in the most frontal position. I planned a new holder which only clamps on the right side of the sheet metal. Maybe it is enough, if you increase the infill so that the holder does not soften. If I am satisfied with my new design I will upload it here.

UPDATE 2017-09-16: I uploaded a new design (filename: Kamerahalter V3.stl). It now clamps at the side of the sheet metal, so it cannot interfere with the rails, even if the material softens.

UPDATE 2018-11-17: I wanted to print a new variant of the holder in PETG. As PETG is pretty sticky I wanted to try to avoid support material. So I splitted the holder into three individual parts which can be glued together. I did not print the new holder yet. When I've done it, I will upload some pictures.

# Print Settings

Printer: Anycubic i3 mega
Rafts: No
Supports: Yes
Resolution: 0,2 mm
Infill: 20%