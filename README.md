# Notes for 3d printing

## Anycubic i3 mega:

Upgraded firmware to [Anycubic i3 Mega Marlin by davidramiro](https://github.com/davidramiro/Marlin-Ai3M)

## Heat settings

With various PLA's from amazon I find that 210 hotend and 60 base works best so 
far.

#### Leveling:

It was fiddly, needed to find a 4gb sd card as all the others I had woudln't 
work. Enter into the secret menu and do mesh leveling, save to eeprom.

TODO: figure out how to get auto leveling mod...

#### After leveling:

just after `G28 Z0` in Cura add

```
M501
M420 S1
```

## OctoPi

Plain setup out of the box, added the firmware addon
